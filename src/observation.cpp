/*
 *  satnogs-kirfs
 *
 *  Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "observation.h"
#include "date.h"

observation::sptr
observation::make_shared(size_t id, size_t station_id, const std::string &tle0,
                         const std::string &tle1, const std::string &tle2,
                         const std::string &transmitter,
                         const std::string &mode,
                         const std::string &start, const std::string &stop,
                         double freq, double baudrate)
{
  return std::shared_ptr<observation>(new observation(id, station_id, tle0, tle1,
                                      tle2, transmitter, mode, start, stop, freq, baudrate));
}

observation::sptr
observation::from_json(nlohmann::json obs)
{
  return make_shared(obs["id"], obs["ground_station"], obs["tle0"], obs["tle1"],
                     obs["tle2"], obs["transmitter"], obs["mode"], obs["start"],
                     obs["end"], obs["frequency"],
                     obs["baud"].is_null() ? 0.0 : obs["baud"].get<double>());
}

observation::observation(size_t id, size_t station_id, const std::string &tle0,
                         const std::string &tle1, const std::string &tle2,
                         const std::string &transmitter,
                         const std::string &mode,
                         const std::string &start, const std::string &stop,
                         double freq, double baudrate) :
  d_id(id),
  d_station_id(station_id),
  d_tle0(tle0),
  d_tle1(tle1),
  d_tle2(tle2),
  d_transmitter(transmitter),
  d_mode(mode),
  d_frequency(freq),
  d_baudrate(baudrate)
{
  try {
    std::istringstream(start) >> date::parse("%FT%TZ", d_start);
    std::istringstream(stop) >> date::parse("%FT%TZ", d_stop);
  }
  catch (std::exception &e) {
    throw std::invalid_argument("observation: Invalid start/stop time");
  }
}

observation::~observation()
{
}

const double
observation::get_baudrate() const
{
  return d_baudrate;
}

const double
observation::get_frequency() const
{
  return d_frequency;
}

const size_t
observation::get_id() const
{
  return d_id;
}

const std::string &
observation::get_mode() const
{
  return d_mode;
}

const size_t
observation::get_station_id() const
{
  return d_station_id;
}


std::vector<std::string>
observation::get_tle()
{
  return {d_tle0, d_tle1, d_tle2};
}

const std::string &
observation::get_transmitter() const
{
  return d_transmitter;
}

std::ostream &operator<<(std::ostream &os, const observation &obs)
{
  os << "Observation " << obs.d_id << std::endl;
  os << "|-> Station ID    : " << obs.d_station_id << std::endl;
  os << "|-> TLE           : " << obs.d_tle0 << std::endl;
  os << "|                   " << obs.d_tle1 << std::endl;
  os << "|                   " << obs.d_tle2 << std::endl;
  os << "|-> Transmitter   : " << obs.d_transmitter << std::endl;
  os << "|-> Mode          : " << obs.d_mode << std::endl;
  os << "|-> Start         : " << date::format("%FT%TZ",
      date::floor<std::chrono::seconds>(obs.d_start)) << std::endl;
  os << "|-> Stop          : " << date::format("%FT%TZ",
      date::floor<std::chrono::seconds>(obs.d_stop)) << std::endl;
  os << "|-> Frequency     : " << obs.d_frequency << std::endl;
  os << "|-> Baudrate      : " << obs.d_baudrate << std::endl;
  return os;
}

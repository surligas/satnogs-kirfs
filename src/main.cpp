/*
 *  satnogs-kirfs
 *
 *  Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <iostream>
#include "network.h"

int
main(int argc, char **argv)
{



  network n = network("network.satnogs.org", 443, 9,
                      "addyourkey");
  std::deque<observation::sptr> d = n.fetch_jobs();
  for (observation::sptr i : d) {
    std::cout << *i << std::endl;
  }
  return 0;
}


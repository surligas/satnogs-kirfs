/*
 *  satnogs-kirfs
 *
 *  Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SRC_OBSERVATION_H_
#define SRC_OBSERVATION_H_

#include <chrono>
#include <string>
#include <memory>
#include <nlohmann/json.hpp>


class observation {
public:
  typedef std::shared_ptr<observation> sptr;

  static sptr
  make_shared(size_t id, size_t station_id,
              const std::string &tle0,
              const std::string &tle1,
              const std::string &tle2,
              const std::string &transmitter,
              const std::string &mode,
              const std::string &start,
              const std::string &stop,
              double freq,
              double baudrate);

  static sptr
  from_json(nlohmann::json obs);

  /* We do not allow copy and default contruction of this object */
  observation() = delete;
  observation(const observation &) = delete;

  ~observation();

  const double
  get_baudrate() const;

  const double
  get_frequency() const;

  const size_t
  get_id() const;

  const std::string &
  get_mode() const;

  const size_t
  get_station_id() const;

  std::vector<std::string>
  get_tle();

  const std::string &
  get_transmitter() const;

  friend std::ostream &operator<<(std::ostream &os, const observation &obs);

protected:
  observation(size_t id, size_t station_id,
              const std::string &tle0,
              const std::string &tle1,
              const std::string &tle2,
              const std::string &transmitter,
              const std::string &mode,
              const std::string &start,
              const std::string &stop,
              double freq,
              double baudrate);
private:
  const size_t d_id;
  const size_t d_station_id;
  const std::string d_tle0;
  const std::string d_tle1;
  const std::string d_tle2;
  const std::string d_transmitter;
  const std::string d_mode;
  const double d_frequency;
  const double d_baudrate;
  std::chrono::system_clock::time_point d_start;
  std::chrono::system_clock::time_point d_stop;
};

#endif /* SRC_OBSERVATION_H_ */

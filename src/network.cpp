/*
 *  satnogs-kirfs
 *
 *  Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define CPPHTTPLIB_OPENSSL_SUPPORT 1

#include "network.h"
#include "httplib.h"
#include <nlohmann/json.hpp>

/**
 * The constructor stores internally all the necessary information required
 * for communicating with the SatNOGS network
 *
 * @note a newly constructed object does not make any network traffic or
 * transaction
 * @param uri the URI of the service (e.g satnogs.org)
 * @param port the port of the service
 * @param station_id the station ID
 * @param key the API key
 */
network::network(const std::string &uri, uint16_t port, size_t station_id,
                 const std::string &key):
  d_uri(uri),
  d_port(port),
  d_station_id(station_id),
  d_key(key)
{
}

network::~network()
{
}

/**
 * Fetch observations from the network
 * @return the observations fetched or an empty queue if none is available
 * or an error occurred
 */
std::deque<observation::sptr>
network::fetch_jobs()
{
  httplib::Headers headers = {
    { "Authorization", "Token " + d_key }
  };
  httplib::SSLClient cli(d_uri, d_port);
  const std::string get_str("/api/jobs/?ground_station=" + std::to_string(
                              d_station_id));
  auto res = cli.Get(get_str.c_str(), headers);
  try {
    std::deque<observation::sptr> q;
    if (res && res->status == 200) {
      nlohmann::json j = nlohmann::json::parse(res->body);
      std::cout << j << std::endl;
      for (auto &i : j) {
        q.push_back(observation::from_json(i));
      }
      return q;
    }
  }
  catch (nlohmann::json::parse_error &e) {
    return {};
  }
  return {};
}

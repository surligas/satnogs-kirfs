/*
 *  satnogs-kirfs
 *
 *  Copyright (C) 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SRC_NETWORK_H_
#define SRC_NETWORK_H_

#include "observation.h"
#include <string>
#include <deque>

/**
 * The network class is responsible for the communication with the SatNOGS
 * Network. This includes fetching observations and transmitter infromation,
 * posting back decoded data, etc.
 */
class network {
public:
  network(const std::string &uri, uint16_t port,
          size_t station_id,
          const std::string &key);

  ~network();

  std::deque<observation::sptr>
  fetch_jobs();

private:
  const std::string d_uri;
  const uint16_t d_port;
  const size_t d_station_id;
  const std::string d_key;
};

#endif /* SRC_NETWORK_H_ */
